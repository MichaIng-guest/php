php7.4 (1:7.4.33-5) unstable; urgency=medium

  * Backported from 8.0.28
   + CVE-2023-0567: Fixed bug #81744 (Password_verify() always return true
     with some hash).
   + CVE-2023-0568: Fixed bug #81746 (1-byte array overrun in common path
     resolve code).
   + CVE-2023-0662: Fixed bug GHSA-54hq-v5wp-fqgv (DOS vulnerability when
     parsing multipart request body).

 -- Ondřej Surý <ondrej@debian.org>  Tue, 14 Feb 2023 18:52:39 +0100

php7.4 (1:7.4.33-3) unstable; urgency=medium

  * Fix GH-10187: Segfault in stripslashes() with arm64

 -- Ondřej Surý <ondrej@debian.org>  Fri, 13 Jan 2023 11:34:42 +0100

php7.4 (1:7.4.33-2) unstable; urgency=medium

  * Backported from 8.0.27
   + CVE-2022-31631: Fixed bug #81740 (PDO::quote() may return unquoted
     string).

 -- Ondřej Surý <ondrej@debian.org>  Fri, 06 Jan 2023 16:32:33 +0100

php7.4 (1:7.4.33-1) unstable; urgency=medium

  * New upstream version 7.4.33
   + CVE-2022-37454: buffer overflow in hash_update() on long parameter
   + CVE-2022-31630: OOB read due to insufficient input validation in
     imageloadfont()

 -- Ondřej Surý <ondrej@debian.org>  Tue, 08 Nov 2022 12:27:39 +0100

php7.4 (1:7.4.32-2) unstable; urgency=medium

  * Backported from PHP 7.4.33
   + CVE-2022-37454: buffer overflow in hash_update() on long parameter

 -- Ondřej Surý <ondrej@debian.org>  Fri, 28 Oct 2022 20:02:29 +0200

php7.4 (1:7.4.32-1) unstable; urgency=medium

  * New upstream version 7.4.32
   + CVE-2022-31628: phar wrapper: DOS when using quine gzip file.
   + CVE-2022-31629: Don't mangle HTTP variable names that clash with
     ones that have a specific semantic meaning.

 -- Ondřej Surý <ondrej@debian.org>  Thu, 29 Sep 2022 23:54:32 +0200

php7.4 (1:7.4.30-6) unstable; urgency=medium

  * Use media-types instead of mime-support (Closes: #1010155)
  * Make the build (mostly) reproducible (Closes: #1001648)
  * Export SED := /bin/sed in d/rules (Closes: #1015188)

 -- Ondřej Surý <ondrej@debian.org>  Sun, 18 Sep 2022 12:04:26 +0200

php7.4 (1:7.4.30-5) unstable; urgency=medium

  * Add upstream patch for OpenSSL 3.0 unexpected EOF failure

 -- Ondřej Surý <ondrej@debian.org>  Mon, 01 Aug 2022 17:05:35 +0200

php7.4 (1:7.4.30-4) unstable; urgency=medium

  * Fix detection of unknown gcc function attributes

 -- Ondřej Surý <ondrej@debian.org>  Thu, 07 Jul 2022 17:50:56 +0200

php7.4 (1:7.4.30-3) unstable; urgency=medium

  * Revert "Add Provides: php-json to PHP SAPIS"

 -- Ondřej Surý <ondrej@debian.org>  Mon, 27 Jun 2022 10:07:43 +0200

php7.4 (1:7.4.30-2) unstable; urgency=medium

  * Add Provides: php-json to PHP SAPIS

 -- Ondřej Surý <ondrej@debian.org>  Sat, 25 Jun 2022 09:57:02 +0200

php7.4 (1:7.4.30-1) unstable; urgency=medium

  * New upstream version 7.4.30

 -- Ondřej Surý <ondrej@debian.org>  Fri, 10 Jun 2022 15:26:25 +0200

php7.4 (1:7.4.29-2) unstable; urgency=medium

  * Add -DOPENSSL_SUPPRESS_DEPRECATED to CFLAGS to support OpenSSL 3.0
  * Add minimal OpenSSL 3.0 patch
  * Pull upstream patch to fix build with ICU >= 70
  * Add #include <stdbool.h> to ext/intl/ to have true/false available
  * Bump the epoch because Ubuntu 22.04 added libapache2-mod-php7.4
    dummy package versioned 8.1.x to help with upgrade and broke this
    package just for just everybody else. Yay!

 -- Ondřej Surý <ondrej@debian.org>  Thu, 28 Apr 2022 13:45:03 +0200

php7.4 (7.4.29-1) unstable; urgency=medium

  * New upstream version 7.4.29

 -- Ondřej Surý <ondrej@debian.org>  Thu, 21 Apr 2022 12:12:21 +0200

php7.4 (7.4.28-1) unstable; urgency=medium

  * New upstream version 7.4.28

 -- Ondřej Surý <ondrej@debian.org>  Thu, 17 Feb 2022 16:51:17 +0100

php7.4 (7.4.27-1) unstable; urgency=medium

  * New upstream version 7.4.27

 -- Ondřej Surý <ondrej@debian.org>  Mon, 20 Dec 2021 22:21:55 +0100

php7.4 (7.4.26-1) unstable; urgency=medium

  * New upstream version 7.4.26

 -- Ondřej Surý <ondrej@debian.org>  Fri, 19 Nov 2021 07:24:55 +0100

php7.4 (7.4.25-3) unstable; urgency=medium

  [ Pino Toscano ]
  * Enable AppArmor (--with-fpm-apparmor) only on Linux archs
  * Fix Vcs-* fields

 -- Ondřej Surý <ondrej@debian.org>  Sat, 13 Nov 2021 12:55:28 +0100

php7.4 (7.4.25-2) unstable; urgency=medium

  * No change bump, to override version in Debian bullseye

 -- Ondřej Surý <ondrej@debian.org>  Mon, 01 Nov 2021 21:00:25 +0100

php7.4 (7.4.25-1) unstable; urgency=medium

  * New upstream version 7.4.25

 -- Ondřej Surý <ondrej@debian.org>  Fri, 22 Oct 2021 14:18:02 +0200

php7.4 (7.4.24-1) unstable; urgency=medium

  * New upstream version 7.4.24

 -- Ondřej Surý <ondrej@debian.org>  Thu, 23 Sep 2021 23:26:56 +0200

php7.4 (7.4.23-1) unstable; urgency=medium

  * New upstream version 7.4.23
  * Check for symlink before removing directory in the postrm scripts

 -- Ondřej Surý <ondrej@debian.org>  Thu, 26 Aug 2021 17:50:19 +0200

php7.4 (7.4.22-1) unstable; urgency=medium

  * New upstream version 7.4.22

 -- Ondřej Surý <ondrej@debian.org>  Fri, 30 Jul 2021 15:04:05 +0200

php7.4 (7.4.21-1) unstable; urgency=medium

  * New upstream version 7.4.21

 -- Ondřej Surý <ondrej@debian.org>  Thu, 01 Jul 2021 17:36:30 +0200

php7.4 (7.4.20-1) unstable; urgency=medium

  * Disable LTO (needed for Ubuntu Hirsute) - now for real
  * New upstream version 7.4.20

 -- Ondřej Surý <ondrej@debian.org>  Fri, 04 Jun 2021 12:11:44 +0200

php7.4 (7.4.18-2) unstable; urgency=medium

  * Disable LTO (needed for Ubuntu Hirsute)
  * Revert: Fix bug #80892 PDO::PARAM_INT on pdo_pgsql

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 May 2021 13:26:07 +0200

php7.4 (7.4.18-1) unstable; urgency=medium

  * New upstream version 7.4.18

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 May 2021 10:58:41 +0200

php7.4 (7.4.16-3) unstable; urgency=medium

  * Allow printing credits buffer larger than 4k

 -- Ondřej Surý <ondrej@debian.org>  Sat, 03 Apr 2021 16:19:26 +0200

php7.4 (7.4.16-2) unstable; urgency=medium

  * Update the packaging credits

 -- Ondřej Surý <ondrej@debian.org>  Thu, 18 Mar 2021 10:36:28 +0100

php7.4 (7.4.16-1) unstable; urgency=medium

  * New upstream version 7.4.16

 -- Ondřej Surý <ondrej@debian.org>  Fri, 05 Mar 2021 08:50:51 +0100

php7.4 (7.4.15-7) unstable; urgency=medium

  * Bump php-common depends to 1:81~

 -- Ondřej Surý <ondrej@debian.org>  Tue, 23 Feb 2021 15:58:09 +0100

php7.4 (7.4.15-6) unstable; urgency=medium

  * Add example configuration to not pass URLs for missing files to
    PHP-FPM

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 17:48:13 +0100

php7.4 (7.4.15-5) unstable; urgency=medium

  * Revert "Don't pass URLs for missing files to PHP-FPM"

 -- Ondřej Surý <ondrej@debian.org>  Fri, 19 Feb 2021 16:33:27 +0100

php7.4 (7.4.15-4) unstable; urgency=medium

  [ Svante Signell ]
  * Add --without build-stamp to dh invocation
  * Add patch to disable HR Timers on GNU Hurd (Closes: 951834)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 19:40:53 +0100

php7.4 (7.4.15-3) unstable; urgency=medium

  * Check if the logrotate script exists (GH #1534)

 -- Ondřej Surý <ondrej@debian.org>  Sun, 14 Feb 2021 15:02:11 +0100

php7.4 (7.4.15-2) unstable; urgency=medium

  [ Sylvain Beucler ]
  * Update obsolete/non-free FPM configuration procedure (Closes: #974034)

  [ Ondřej Surý ]
  * Enable FPM ACL support (Closes: #982574)

  [ Kevin Locke ]
  * Don't pass URLs for missing files to PHP-FPM (Closes :#954850)

 -- Ondřej Surý <ondrej@debian.org>  Sat, 13 Feb 2021 20:05:33 +0100

php7.4 (7.4.15-1) unstable; urgency=medium

  * New upstream version 7.4.15
  * Force hardcoded path to be /bin/sed (Closes: #960786)

 -- Ondřej Surý <ondrej@debian.org>  Sun, 07 Feb 2021 12:36:49 +0100

php7.4 (7.4.14-1) unstable; urgency=medium

  * New upstream version 7.4.14

 -- Ondřej Surý <ondrej@debian.org>  Tue, 12 Jan 2021 11:26:28 +0100

php7.4 (7.4.13-1) unstable; urgency=medium

  * New upstream version 7.4.13

 -- Ondřej Surý <ondrej@debian.org>  Sat, 28 Nov 2020 07:20:33 +0100

php7.4 (7.4.12-3) unstable; urgency=medium

  * Copy the files from auxdir in a separate variable to sync with PHP 7.3
    and lower

 -- Ondřej Surý <ondrej@debian.org>  Sat, 31 Oct 2020 18:03:15 +0100

php7.4 (7.4.12-2) unstable; urgency=medium

  * Move the non-m4 files from LIBTOOL_FILES to FILES_BUILD

 -- Ondřej Surý <ondrej@debian.org>  Sat, 31 Oct 2020 11:03:35 +0100

php7.4 (7.4.12-1) unstable; urgency=medium

  * New upstream version 7.4.12
  * Move the system wide phpize files to LIBTOOL_FILES

 -- Ondřej Surý <ondrej@debian.org>  Fri, 30 Oct 2020 20:45:32 +0100

php7.4 (7.4.11-6) unstable; urgency=medium

  * Fix typo in phpize script

 -- Ondřej Surý <ondrej@debian.org>  Sun, 18 Oct 2020 21:38:34 +0200

php7.4 (7.4.11-5) unstable; urgency=medium

  * In phpize, copy the foreign files from their respective packages
    (libtool, pkg-config, shtool)

 -- Ondřej Surý <ondrej@debian.org>  Sun, 18 Oct 2020 20:44:09 +0200

php7.4 (7.4.11-4) unstable; urgency=medium

  * Include all libtool files from phpize.m4

 -- Ondřej Surý <ondrej@debian.org>  Sun, 18 Oct 2020 13:32:38 +0200

php7.4 (7.4.11-3) unstable; urgency=medium

  [ Chris Hofstaedtler ]
  * Use netcat-openbsd to build instead of netcat-traditional (Closes: #963261)

  [ Pino Toscano ]
  * Disable AppArmor support on non-Linux archs (Closes: #951857)
  * Enable systemd integration only on Linux archs (Closes: #951834)

  [ Ondřej Surý ]
  * Use system-wide pkg.m4 from pkg-config package in phpize
  * Restore the patch to use system-wide libtool and pkg-config m4 files

 -- Ondřej Surý <ondrej@debian.org>  Sat, 17 Oct 2020 08:15:26 +0200

php7.4 (7.4.11-2) unstable; urgency=medium

  * Disable the MySQL extension testing as it's too complicated

 -- Ondřej Surý <ondrej@debian.org>  Sat, 10 Oct 2020 21:44:18 +0200

php7.4 (7.4.11-1) unstable; urgency=medium

  * New upstream version 7.4.11

 -- Ondřej Surý <ondrej@debian.org>  Tue, 06 Oct 2020 12:34:39 +0200

php7.4 (7.4.10-1) unstable; urgency=medium

  * New upstream version 7.4.10
  * Lower the minimal debhelper dependency to >= 9.20150101~
  * Use libenchant-dev as Build-Depends alternative to libenchant-2-dev
  * Remove deprecated calls from enchant-2 (Closes: #954855)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 08 Sep 2020 12:22:27 +0200

php7.4 (7.4.9-2) unstable; urgency=medium

  * Pull upstream patch for enchant-2 and change build-dep (Closes: #954855)

 -- Ondřej Surý <ondrej@debian.org>  Thu, 27 Aug 2020 15:37:23 +0200

php7.4 (7.4.9-1) unstable; urgency=medium

  * New upstream version 7.4.9

 -- Ondřej Surý <ondrej@debian.org>  Fri, 07 Aug 2020 16:27:40 +0200

php7.4 (7.4.8-1) unstable; urgency=medium

  * Finish updating the packaging to dh compat level 10
  * New upstream version 7.4.8
  * Adjust for upstream phar.phar -> phar7.4.phar binary rename

 -- Ondřej Surý <ondrej@debian.org>  Mon, 13 Jul 2020 18:35:53 +0200

php7.4 (7.4.7-1) unstable; urgency=medium

  * New upstream version 7.4.7

 -- Ondřej Surý <ondrej@debian.org>  Fri, 12 Jun 2020 09:42:27 +0200

php7.4 (7.4.6-1) unstable; urgency=medium

  * Properly detect CRC32 APIs on aarch64 from configure
  * New upstream version 7.4.6

 -- Ondřej Surý <ondrej@debian.org>  Thu, 14 May 2020 11:59:59 +0200

php7.4 (7.4.5-1) unstable; urgency=medium

  * New upstream version 7.4.5

 -- Ondřej Surý <ondrej@debian.org>  Sun, 19 Apr 2020 09:35:13 +0200

php7.4 (7.4.4-1) unstable; urgency=medium

  * Add (non-existent yet) systemd-tmpfiles package as alternative to systemd
  * php-fpm has to depend on procps due kill usage in systemd service file
    (Closes: #861855)
  * New upstream version 7.4.4

 -- Ondřej Surý <ondrej@debian.org>  Fri, 20 Mar 2020 14:45:16 +0100

php7.4 (7.4.3-4) unstable; urgency=medium

  * Remove /etc/init/php@PHP_VERSION@-fpm.conf, not
    /etc/init/php@PHP_VERSION@.conf (Closes: #951745)

 -- Ondřej Surý <ondrej@debian.org>  Sun, 23 Feb 2020 08:07:28 +0100

php7.4 (7.4.3-3) unstable; urgency=medium

  * Fixup upstart removal (missing prepare-files update) (Closes: #951745)

 -- Ondřej Surý <ondrej@debian.org>  Fri, 21 Feb 2020 18:01:35 +0100

php7.4 (7.4.3-2) unstable; urgency=medium

  * Remove the PIDFile= setting from systemd unit file (it should not be
    needed with Type=notify)
  * Use php-fpm-socket-helper from php-common >= 1:73 to update the
    default socket

 -- Ondřej Surý <ondrej@debian.org>  Fri, 21 Feb 2020 09:59:48 +0100

php7.4 (7.4.3-1) unstable; urgency=medium

  * Remove upstart support, use systemd-tmpfiles to create tmpfiles
    (Closes: #923032)
  * New upstream version 7.4.3

 -- Ondřej Surý <ondrej@debian.org>  Thu, 20 Feb 2020 13:12:06 +0100

php7.4 (7.4.2-7) unstable; urgency=medium

  * Add a note about PIDFile= and pid= match in php-fpm.conf
  * Silently ignore errors from update-alternatives in php-fpm.service

 -- Ondřej Surý <ondrej@debian.org>  Sat, 08 Feb 2020 13:04:50 +0100

php7.4 (7.4.2-6) unstable; urgency=medium

  * Use absolute path to update-alternatives

 -- Ondřej Surý <ondrej@debian.org>  Wed, 05 Feb 2020 17:47:54 +0100

php7.4 (7.4.2-5) unstable; urgency=medium

  * Move the update-alternatives call from postinst/prerm to systemd startup script

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Feb 2020 18:44:05 +0100

php7.4 (7.4.2-4) unstable; urgency=medium

  * Make the creation of the default socket work on new installs

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Feb 2020 14:11:48 +0100

php7.4 (7.4.2-3) unstable; urgency=medium

  * Use a mock socket file for setting up FPM socket alternatives

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Feb 2020 13:09:39 +0100

php7.4 (7.4.2-2) unstable; urgency=medium

  * Create a generic /run/php/php-fpm.sock socket using update-alternatives

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Feb 2020 10:17:27 +0100

php7.4 (7.4.2-1) unstable; urgency=medium

  * New upstream version 7.4.2
  * Disable dh_autoreconf for PHP, it breaks the build

 -- Ondřej Surý <ondrej@debian.org>  Thu, 23 Jan 2020 12:20:45 +0100

php7.4 (7.4.1-1) unstable; urgency=medium

  * Update d/watch for final release
  * New upstream version 7.4.1
  * Bump the debhelper compat to 10
  * Bump the Standards Version (no change)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 21 Jan 2020 09:23:37 +0100

php7.4 (7.4.0-1) unstable; urgency=medium

  * New upstream version 7.4.0

 -- Ondřej Surý <ondrej@debian.org>  Thu, 28 Nov 2019 08:25:29 +0100

php7.4 (7.4.0~rc6-1) experimental; urgency=medium

  * Fix the FTBFS with MySQL 8.0
  * New upstream version 7.4.0~rc6

 -- Ondřej Surý <ondrej@debian.org>  Tue, 19 Nov 2019 18:49:28 +0100

php7.4 (7.4.0~rc4-1) experimental; urgency=medium

  * Bump d/phpapi to 20190902
  * New upstream version 7.4.0~rc4

 -- Ondřej Surý <ondrej@debian.org>  Sat, 26 Oct 2019 11:10:04 +0200

php7.4 (7.4.0~rc3-1) experimental; urgency=medium

  * New upstream version 7.4.0~rc3
  * GMP now uses autodetection (don't pass /usr to configure)
  * Bump d/phpapi to 20190902

 -- Ondřej Surý <ondrej@sury.org>  Tue, 08 Oct 2019 08:08:28 +0200

php7.4 (7.4.0~beta4-2) experimental; urgency=medium

  * Enable FFI experimental extension
  * Add libffi to B-D

 -- Ondřej Surý <ondrej@sury.org>  Wed, 28 Aug 2019 10:50:48 +0200

php7.4 (7.4.0~beta4-1) experimental; urgency=medium

  * Remove 0003-libtool2.2.patch, it's no longer needed [GL #1236]
  * New upstream version 7.4.0~beta4

 -- Ondřej Surý <ondrej@sury.org>  Tue, 27 Aug 2019 15:22:26 +0200

php7.4 (7.4.0~beta2-1) experimental; urgency=medium

  * New upstream version 7.4.0~beta2
  * Rebase patches for PHP 7.4.0~beta2

 -- Ondřej Surý <ondrej@sury.org>  Thu, 08 Aug 2019 13:41:59 +0200

php7.4 (7.4.0~beta1-1) experimental; urgency=medium

  * New upstream version 7.4.0~beta1
  * Rebase patches for PHP 7.4.0~beta1
  * Configure option --with-libxml-dir is now named --with-libxml
  * The recode extension has been moved to PECL.
  * The interbase extension has been moved to PECL.
  * The configure option for zip extension has changed from --enable-zip to --with-zlib
  * The WDDX extension has been deprecated and moved to PECL.
  * The configure options to enable GD extension has changed to --enable-gd and --with-external-gd
  * Regenerated d/control
  * Update the configure options according to UPGRADING file (mostly pkg-config related changes)
  * Cleanup the missing documentation
  * Update phpapi to 20190529

 -- Ondřej Surý <ondrej@sury.org>  Wed, 07 Aug 2019 17:47:41 +0200

php7.4 (7.4.0~alpha2-1) experimental; urgency=low

  * New upstream version 7.4.0~alpha2

 -- Ondřej Surý <ondrej@sury.org>  Wed, 10 Jul 2019 09:36:25 +0200

php7.3 (7.3.7-1) unstable; urgency=medium

  * New upstream version 7.3.7

 -- Ondřej Surý <ondrej@sury.org>  Wed, 10 Jul 2019 08:52:54 +0200

php7.3 (7.3.6-1) unstable; urgency=medium

  [ Ondřej Surý ]
  * New upstream version 7.3.6

  [ Andreas Beckmann ]
  * php7.3-curl: Add Breaks against php7.0-curl for smoother upgrades from stretch.  (Closes: #929689)

 -- Ondřej Surý <ondrej@sury.org>  Fri, 31 May 2019 13:36:51 +0200

php7.3 (7.3.5-1) unstable; urgency=medium

  * New upstream version 7.3.5

 -- Ondřej Surý <ondrej@sury.org>  Fri, 03 May 2019 10:16:15 +0200

php7.3 (7.3.4-2) unstable; urgency=medium

  [Andreas Beckmann]
  * php7.3-common: Add Breaks against php7.0-curl for smoother upgrades from
    stretch.  (Closes: #925106)
  * php7.3-common: Add Breaks against gforge-common from jessie which uses a
    deprecated constructor syntax.
  * Deterministically generate debian/control by sorting the extension
    packages.

 -- Ondřej Surý <ondrej@debian.org>  Sat, 13 Apr 2019 19:05:48 +0000

php7.3 (7.3.4-1) unstable; urgency=medium

  * Update d/watch for new php.net pages
  * New upstream version 7.3.4
  * Enforce C++11 for intl compilation on older distributions

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Apr 2019 06:55:43 +0000

php7.3 (7.3.3-1) unstable; urgency=medium

  * New upstream version 7.3.3
  * Update systzdata patch to v18 (Courtesy of RemiRepo)
  * Add patch for OpenSSL 1.1.1b (Courtesy of RemiRepo)

 -- Ondřej Surý <ondrej@debian.org>  Thu, 07 Mar 2019 19:43:34 +0000

php7.3 (7.3.2-3) unstable; urgency=medium

  * Update systzdata patch to v17 (Courtesy of remirepo)

 -- Ondřej Surý <ondrej@debian.org>  Fri, 08 Feb 2019 15:05:54 +0000

php7.3 (7.3.2-2) unstable; urgency=medium

  * Fix the icu patch condition for icu >= 60

 -- Ondřej Surý <ondrej@debian.org>  Fri, 08 Feb 2019 10:49:26 +0000

php7.3 (7.3.2-1) unstable; urgency=medium

  * New upstream version 7.3.2

 -- Ondřej Surý <ondrej@debian.org>  Thu, 07 Feb 2019 17:58:05 +0000

php7.3 (7.3.1-3) unstable; urgency=medium

  * Always build spoofchecker, because we are enforcing icu >= 50.1
    (Closes: #921199)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 05 Feb 2019 10:25:33 +0000

php7.3 (7.3.1-2) unstable; urgency=high

  * Add patch to use pkg-config instead of icu-config to detect icu
    libraries (Closes: #916110)

 -- Ondřej Surý <ondrej@debian.org>  Mon, 21 Jan 2019 09:09:55 +0000

php7.3 (7.3.1-1) unstable; urgency=medium

  * New upstream version 7.3.1

 -- Ondřej Surý <ondrej@debian.org>  Sun, 13 Jan 2019 10:13:20 +0000

php7.3 (7.3.0-2) unstable; urgency=medium

  * Add upstream patch to fix OPcache optimization problem for
    ArrayAccess->offsetGet
  * Add upstream patch to fix infinite loop in preg_replace_callback
  * Fix check for rl_completion_matches in readline extension

 -- Ondřej Surý <ondrej@debian.org>  Mon, 17 Dec 2018 09:51:53 +0000

php7.3 (7.3.0-1) unstable; urgency=medium

  * Update d/watch for the final PHP 7.3.0 release
  * New upstream version 7.3.0

 -- Ondřej Surý <ondrej@debian.org>  Thu, 06 Dec 2018 20:22:15 +0000

php7.3 (7.3.0~rc6-1) unstable; urgency=medium

  * New upstream version 7.3.0~rc6

 -- Ondřej Surý <ondrej@debian.org>  Sun, 25 Nov 2018 10:01:25 +0000

php7.3 (7.3.0~rc5-2) unstable; urgency=medium

  * Don't use sed found by configure, use the sed command as available in
    the host system (Closes: #913620)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 13 Nov 2018 09:10:56 +0000

php7.3 (7.3.0~rc5-1) unstable; urgency=medium

  * New upstream version 7.3.0~rc5
  * Enable lmdb support in dba extension

 -- Ondřej Surý <ondrej@debian.org>  Mon, 12 Nov 2018 09:54:24 +0000

php7.3 (7.3.0~rc4-2) unstable; urgency=medium

  * Restore correct patch name for
    0040-Add-patch-to-install-php7-module-directly-to-APXS_LI.patch

 -- Ondřej Surý <ondrej@debian.org>  Sun, 04 Nov 2018 04:54:20 +0000

php7.3 (7.3.0~rc4-1) unstable; urgency=medium

  * New upstream version 7.3.0~rc4
  * Rebase patches for PHP 7.4.0~rc4

 -- Ondřej Surý <ondrej@debian.org>  Thu, 25 Oct 2018 08:57:33 +0000

php7.3 (7.3.0~rc3-3) unstable; urgency=medium

  * Add patch to use pkg-config for FreeType2 library detection
    (Closes: #911460)
  * Remove libmcrypt-dev from Build-Depends

 -- Ondřej Surý <ondrej@debian.org>  Thu, 25 Oct 2018 06:39:32 +0000

php7.3 (7.3.0~rc3-2) unstable; urgency=medium

  * Disable the enabled modules in prerm, because in postrm the phpquery
    script is not aware of already removed sapi (Closes: #911018)

 -- Ondřej Surý <ondrej@debian.org>  Mon, 15 Oct 2018 09:53:04 +0000

php7.3 (7.3.0~rc3-1) unstable; urgency=medium

  * New upstream version 7.3.0~rc3
  * Rebase patches for PHP 7.3.0~rc3

 -- Ondřej Surý <ondrej@debian.org>  Sat, 13 Oct 2018 13:47:36 +0000

php7.3 (7.3.0~rc2-3) unstable; urgency=medium

  * Remove ancient mv_conffile (from php5)
  * Remove spurious L from phpize script (Closes: #909110)
  * Downgrade dh-php from Recommends to Suggests (Closes: #910620)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 09 Oct 2018 13:22:52 +0000

php7.3 (7.3.0~rc2-2) unstable; urgency=medium

  * Fix the Vcs-* links
  * Apply upstream patch to allow disabling pcre jit and disable it on
    mips and s390x archs
  * Extra 'L' is gone (Closes: #909110)

 -- Ondřej Surý <ondrej@debian.org>  Thu, 04 Oct 2018 14:25:15 +0000

php7.3 (7.3.0~rc2-1) unstable; urgency=medium

  * New upstream version 7.3.0~rc2
  * Rebase patches for PHP 7.3.0~rc2

 -- Ondřej Surý <ondrej@debian.org>  Mon, 01 Oct 2018 11:42:35 +0000

php7.3 (7.3.0~beta2-3) unstable; urgency=medium

  * Disable assembly code with gcc 4.8 on i386

 -- Ondřej Surý <ondrej@debian.org>  Mon, 20 Aug 2018 08:07:58 +0000

php7.3 (7.3.0~beta2-2) unstable; urgency=medium

  * Remove dependency on pcre3 and add libpcre2-dev to phpX.Y-dev

 -- Ondřej Surý <ondrej@debian.org>  Sun, 19 Aug 2018 16:12:50 +0000

php7.3 (7.3.0~beta2-1) unstable; urgency=medium

  * New upstream version 7.3.0~beta2
  * Rebase patches for PHP 7.3.0~beta2
  * Fix phpdbg.1 installation path from srcdir to builddir
  * Bump d/phpapi to 20180731

 -- Ondřej Surý <ondrej@debian.org>  Sun, 19 Aug 2018 07:49:10 +0000

php7.3 (7.3.0~beta1-1) unstable; urgency=medium

  [ Lior Kaplan ]
  * Fix syntax typo

  [ Ondřej Surý ]
  * New upstream version 7.3.0~beta1
  * Rebase patches for PHP 7.3.0beta1

 -- Ondřej Surý <ondrej@debian.org>  Fri, 03 Aug 2018 13:52:09 +0000

php7.3 (7.3.0~alpha4-1) unstable; urgency=medium

  * Use cpuid.h instead of custom assembler
  * New upstream version 7.3.0~alpha4
  * Rebase patches for PHP 7.3.0~alpha4

 -- Ondřej Surý <ondrej@debian.org>  Wed, 25 Jul 2018 11:11:09 +0000

php7.3 (7.3.0~alpha3-2) unstable; urgency=medium

  * Remove traces of ext_skel modifications
  * Add <!nocheck> profile to all default-mysql-server alternatives
  * Bump d/phpapi for PHP 7.3
  * Add libargon2-dev as new alternative build-dependency to
    libargon2-0-dev

 -- Ondřej Surý <ondrej@debian.org>  Sat, 14 Jul 2018 13:57:34 +0000

php7.3 (7.3.0~alpha3-1) unstable; urgency=medium

  * Update upstream signing-key.asc for PHP 7.3
  * New upstream version 7.3.0~alpha3
  * Build-Depend on libpcre2-dev
  * Rebase patches for PHP 7.3.0~alpha3

 -- Ondřej Surý <ondrej@debian.org>  Mon, 09 Jul 2018 13:49:59 +0000

php7.2 (7.2.7-2) unstable; urgency=medium

  * Update the maintainer email to team+pkg-php@tracker.debian.org
  * Update the Vcs-* links to salsa.d.o

 -- Ondřej Surý <ondrej@debian.org>  Mon, 09 Jul 2018 12:28:45 +0000

php7.2 (7.2.7-1) unstable; urgency=medium

  * New upstream version 7.2.7
  * Refresh patches for PHP 7.2.7

 -- Ondřej Surý <ondrej@debian.org>  Fri, 22 Jun 2018 07:35:11 +0000

php7.2 (7.2.6-1) unstable; urgency=medium

  * New upstream version 7.2.6
  * Rebase patches for PHP version 7.2.6

 -- Ondřej Surý <ondrej@debian.org>  Mon, 11 Jun 2018 14:54:56 +0000

php7.2 (7.2.5-1) unstable; urgency=medium

  * New upstream version 7.2.5
  * Rebase patches for PHP 7.2.5

 -- Ondřej Surý <ondrej@debian.org>  Sat, 05 May 2018 04:56:32 +0000

php7.2 (7.2.4-1) unstable; urgency=medium

  * New upstream version 7.2.4
  * Rebase patches on top of new upstream release.

 -- Ondřej Surý <ondrej@debian.org>  Thu, 05 Apr 2018 08:50:27 +0000

php7.2 (7.2.3-1) unstable; urgency=medium

  * New upstream version 7.2.3
  * Rebase patches on top of new upstream release.

 -- Ondřej Surý <ondrej@debian.org>  Tue, 06 Mar 2018 11:15:04 +0000

php7.2 (7.2.2-3) unstable; urgency=medium

  * Add explicit libpcre3 >= 2:8.35 dependency as dh_genshlibs is failing
    to add versioned dependency for some reason.

 -- Ondřej Surý <ondrej@debian.org>  Tue, 06 Feb 2018 16:07:40 +0000

php7.2 (7.2.2-2) unstable; urgency=medium

  * Remove explicit libpcre3 dependency and let dh_genshlibs do its magic

 -- Ondřej Surý <ondrej@debian.org>  Tue, 06 Feb 2018 13:00:04 +0000

php7.2 (7.2.2-1) unstable; urgency=medium

  * New upstream version 7.2.2
  * Rebase patches on top of new upstream release
  * Regenerate d/control to finish php7.2-sodium removal

 -- Ondřej Surý <ondrej@debian.org>  Thu, 01 Feb 2018 15:19:04 +0000

php7.2 (7.2.1-1) unstable; urgency=medium

  * Update the Vcs-* to salsa.d.o
  * Slightly update debian/copyright (most changes were already in)
  * New upstream version 7.2.1
  * Rebase patches on top of new upstream release

 -- Ondřej Surý <ondrej@debian.org>  Fri, 05 Jan 2018 11:21:04 +0000

php7.2 (7.2.0-2) unstable; urgency=medium

  * Get rid of extra php7.2-sodium module

 -- Ondřej Surý <ondrej@debian.org>  Wed, 06 Dec 2017 14:15:47 +0000

php7.2 (7.2.0-1) unstable; urgency=low

  * Update PHP 7.2 signing keys
  * New upstream version 7.2.0
  * Rebase patches for new upstream release.

 -- Ondřej Surý <ondrej@debian.org>  Thu, 30 Nov 2017 13:55:57 +0000

php7.2 (7.2.0~rc6-1) unstable; urgency=medium

  * New upstream version 7.2.0~rc6
  * Rebase patches for new upstream version.

 -- Ondřej Surý <ondrej@debian.org>  Sun, 12 Nov 2017 03:30:05 +0000

php7.2 (7.2.0~rc5-1) unstable; urgency=medium

  * New upstream version 7.2.0~rc5
  * Rebase patches for new upstream release

 -- Ondřej Surý <ondrej@debian.org>  Fri, 27 Oct 2017 13:33:55 +0000

php7.2 (7.2.0~rc4-2) unstable; urgency=medium

  * Fix the usage of internal allocator in xmlrpc extension

 -- Ondřej Surý <ondrej@debian.org>  Tue, 24 Oct 2017 18:54:46 +0000

php7.2 (7.2.0~rc4-1) unstable; urgency=medium

  * New upstream version 7.2.0~rc4
  * Rebase patches on top of new upstream version 7.2.0~rc4

 -- Ondřej Surý <ondrej@debian.org>  Sun, 22 Oct 2017 13:07:11 +0000

php7.2 (7.2.0~rc3-1) unstable; urgency=medium

  * New upstream version 7.2.0~rc3
  * Refresh patches for PHP 7.2.0~rc3

 -- Ondřej Surý <ondrej@debian.org>  Thu, 28 Sep 2017 18:26:49 +0200

php7.2 (7.2.0~rc2-1) unstable; urgency=medium

  * New upstream version 7.2.0~rc2
  * Rebase patches on top of PHP 7.2.0~rc2

 -- Ondřej Surý <ondrej@debian.org>  Mon, 18 Sep 2017 11:24:14 +0200

php7.2 (7.2.0~rc1-1) unstable; urgency=medium

  * New upstream version 7.2.0~rc1
  * Rebase patches on top of PHP 7.2.0~rc1
  * Update d/copyright (License check courtesy of Luca Falavigna)
  * Rewrap the files in d/ with wrap-and-sort -a

 -- Ondřej Surý <ondrej@debian.org>  Thu, 31 Aug 2017 14:00:16 +0200

php7.2 (7.2.0~beta3-2) unstable; urgency=medium

  * Enable Argon2 support for password hashing functions
  * Enable shared libsodium extension

 -- Ondřej Surý <ondrej@debian.org>  Fri, 25 Aug 2017 11:35:23 +0200

php7.2 (7.2.0~beta3-1) unstable; urgency=medium

  * Allow libgcrypt11-dev when it's not a transitional package
  * New upstream version 7.2.0~beta3
  * Refresh patches on top of PHP 7.2.0~beta3

 -- Ondřej Surý <ondrej@debian.org>  Fri, 18 Aug 2017 15:00:36 +0200

php7.2 (7.2.0~beta2-2) experimental; urgency=medium

  * Update Vcs-* links to https://gitlab.com/deb.sury.org/...
  * Stop depending on obsolete automake1.11
  * Switch build-depends to libgcrypt20-dev

 -- Ondřej Surý <ondrej@debian.org>  Fri, 04 Aug 2017 11:56:09 +0200

php7.2 (7.2.0~beta2-1) experimental; urgency=medium

  * Update d/watch for PHP 7.2
  * New upstream version 7.2.0~beta2
  * Rebase patches for PHP 7.2.0~beta2

 -- Ondřej Surý <ondrej@debian.org>  Thu, 03 Aug 2017 20:42:38 +0200

php7.2 (7.2.0~beta1-1) experimental; urgency=medium

  * New upstream version 7.2.0~beta1
  * Enable support for libsodium crypto
  * Rebase patches on top of PHP 7.2.0~beta1
  * Update phpapi for PHP 7.2 to 20170718

 -- Ondřej Surý <ondrej@debian.org>  Thu, 27 Jul 2017 13:29:34 +0200

php7.2 (7.2.0~alpha3-1) experimental; urgency=medium

  * New upstream version 7.2.0~alpha3
  * Rebase patches on top of PHP 7.2.0~alpha3
  * Update d/rules with configure.in -> configure.ac rename
  * Remove mcrypt extension that has been removed upstream
  * Update phpapi to 20160731

 -- Ondřej Surý <ondrej@debian.org>  Thu, 06 Jul 2017 13:50:44 +0200
